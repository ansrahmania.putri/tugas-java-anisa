package tugas2;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
    static Scanner input = new Scanner(System.in);
    static ArrayList<Model> mhs = new ArrayList<>();
    static DataOutputStream dout;
    static Socket s;

    static int choice;
    static String inputIP, inputPort, str;

    public static void showMenu() {
        System.out.println("\nMenu");
        System.out.println("1. Connect Socket");
        System.out.println("2. Create File Process");
        System.out.println("3. Student's Score Average");
        System.out.println("4. Disconnected");
        System.out.println("0. Exit");

        System.out.print("Pilih menu: ");
        choice = input.nextInt();
    }
    public static void readConfig() {
        try (InputStream inp = new FileInputStream("/Users/ada-nb187/Documents/tugasd8/src/tugas2/config.properties")) {

            Properties prop = new Properties();

            prop.load(inp);

            JSONObject config = new JSONObject();
            inputIP = prop.getProperty("IP");
            config.put("Port",inputPort);
            inputPort = prop.getProperty("Port");
            config.put("IP",inputIP);
            System.out.print(config);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void clientSocket() throws IOException {
        System.out.println("\n[Connect Client Socket]");

        s = new Socket(inputIP, Integer.parseInt(inputPort));

        //Send
        dout = new DataOutputStream(s.getOutputStream());
        dout.writeUTF("Get Data");
        dout.flush();

        //Receive
        DataInputStream dis = new DataInputStream(s.getInputStream());
        str = dis.readUTF();
        System.out.println("Server: \n"+str);
    }

    public static void parsingData() {
        System.out.println("\n[Parsing Data]");

        String[] strAccuParsed = str.split("\n");

        for(int j = 0; j < strAccuParsed.length; j++) {
            System.out.println(strAccuParsed[j]);
            if(j > 0) {
                String[] strAccuParsed2 = strAccuParsed[j].trim().split(",");

                String nama = strAccuParsed2[0];
                double fisika = Integer.parseInt(strAccuParsed2[1]);
                double kimia = Integer.parseInt(strAccuParsed2[2]);
                double biologi = Integer.parseInt(strAccuParsed2[3]);


                Model s = new Model(nama, fisika, kimia, biologi);
                mhs.add(s);
            }
        }
    }

    public static void decodeJSON () throws IOException, ParseException {

        JSONParser parser = new JSONParser();
        Reader reader = new StringReader(str);

        Object jsonObj = parser.parse(reader);

        JSONObject jsonObject = (JSONObject) jsonObj;

        ArrayList<JSONObject> alMhs = (ArrayList<JSONObject>) jsonObject.get("data");
        for (JSONObject elMhs : alMhs) {
            String namaMhs = (String) elMhs.get("nama");
            System.out.println("Nama = " + namaMhs);
            double fisikaMhs = (double) elMhs.get("fisika");
            System.out.println("Fisika = " + fisikaMhs);
            double kimiaMhs = (double) elMhs.get("kimia");
            System.out.println("Kimia = " + kimiaMhs);
            double bioMhs = (double) elMhs.get("biologi");
            System.out.println("Biologi = " + bioMhs);
            System.out.println();

            Model m = new Model(namaMhs, fisikaMhs, kimiaMhs, bioMhs);
            mhs.add(m);
        }
    }

    public static void addNewFile() {
        System.out.println("\n[Tambah Data ke File Baru]");

        String newFile = "/Users/ada-nb187/Documents/tugasd8/src/tugas2/fileWriter.txt";

        try{
            FileWriter fw = new FileWriter(newFile);
            decodeJSON();
            for (Model m : mhs) {
                fw.write("Nama: "+m.getNama()+"\nNilai Fisika: "+m.getNilaiFis()+"\nNilai Kimia: "+m.getNilaiKim()+"\nNilai Biologi: "+m.getNilaiBio()+"\n\n");
            }
            fw.close();
        }catch(Exception e){
            System.out.println(e);
        }

        System.out.println("Success...");
    }

    public static void averageNilai() {
        String fw = new String("/Users/ada-nb187/Documents/tugasd8/src/tugas2/fileWriter.txt");

        try {
            FileWriter fileBaru = new FileWriter(fw);
            fileBaru.write("Nama, averageNilai");
            for (Model el: mhs) {

                double fis = el.getNilaiFis();
                double kim = el.getNilaiKim();
                double bio = el.getNilaiBio();

                double sumNilai = fis + kim + bio;
                double avgNilai = (sumNilai)/3;

                fileBaru.write("\n"+ el.getNama() + "," + avgNilai + "\n");
            }
            fileBaru.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        try{
            readConfig();
            do {
                showMenu();
                switch (choice) {
                    case 1 :
                        clientSocket();
                        parsingData();
                        break;
                    case 2 :
                        addNewFile();
                        break;
                    case 3:
                        averageNilai();
                        break;
                    case 4 :
                        dout.close();
                        s.close();
                        System.out.println("Koneksi Terputus");
                        break;
                    default:
                        if (choice == 0) {
                            break;
                        } else {
                            System.out.println("Menu yang anda pilih tidak ditemukan");
                        }
                }
            } while (choice != 0);
            dout.close();
            s.close();
        }catch(Exception e){
            System.out.println(e);
        }

        input.close();
    }
}
