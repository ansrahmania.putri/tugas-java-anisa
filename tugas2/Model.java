package tugas2;

public class Model {

    String nama;
    double nilaiFis;
    double nilaiKim;
    double nilaiBio;

    public Model(String nama, double nilaiFis, double nilaiKim, double nilaiBio) {
        this.nama = nama;
        this.nilaiFis = nilaiFis;
        this.nilaiKim = nilaiKim;
        this.nilaiBio = nilaiBio;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getNilaiFis() {
        return nilaiFis;
    }

    public void setNilaiFis(double nilaiFis) {
        this.nilaiFis = nilaiFis;
    }

    public double getNilaiKim() {
        return nilaiKim;
    }

    public void setNilaiKim(double nilaiKim) {
        this.nilaiKim = nilaiKim;
    }

    public double getNilaiBio() {
        return nilaiBio;
    }

    public void setNilaiBio(double nilaiBio) {
        this.nilaiBio = nilaiBio;
    }
}
