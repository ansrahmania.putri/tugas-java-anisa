package tugas2;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
    static Scanner input = new Scanner(System.in);
    static ArrayList<Model> mhs = new ArrayList<>();
    static String inputPort;
    static ServerSocket ss;
    static String[] strAccuParsed;
    static StringBuilder strAccu;
    static JSONObject data;
    static String nama;
    static double fisika, kimia, biologi;
    static JSONArray arrayMhs;
    static JSONObject allObj;
    public static void readConfig() {
        try (InputStream inp = new FileInputStream("/Users/ada-nb187/Documents/tugasd8/src/tugas2/config.properties")) {

            Properties prop = new Properties();

            prop.load(inp);

            JSONObject config = new JSONObject();
            inputPort = prop.getProperty("Port");
            config.put("port",inputPort);
            System.out.print(config);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void readData() throws IOException {
        System.out.println("\n\n[Parsing Data]");

        String file = "/Users/ada-nb187/Documents/tugasd8/src/tugas2/file.txt";

        FileReader fr = new FileReader(file);

        int i;
        strAccu = new StringBuilder();
        while((i=fr.read()) != -1) {
            strAccu.append((char) i);
        }

        strAccuParsed = strAccu.toString().split("\n");

        allObj = new JSONObject();
        arrayMhs = new JSONArray();
        for(int j = 0; j < strAccuParsed.length; j++) {
            if(j > 0) {
                String[] strAccuParsed2 = strAccuParsed[j].trim().split(",");

                nama = strAccuParsed2[0];
                fisika = Integer.parseInt(strAccuParsed2[1]);
                kimia = Integer.parseInt(strAccuParsed2[2]);
                biologi = Integer.parseInt(strAccuParsed2[3]);

                data = new JSONObject();
                data.put("nama", nama);
                data.put("fisika", fisika);
                data.put("kimia", kimia);
                data.put("biologi", biologi);

                arrayMhs.add(data);

                Model s = new Model(nama, fisika, kimia, biologi);
                mhs.add(s);
            }
        }allObj.put("data", arrayMhs);
        System.out.println(allObj);
        fr.close();
    }
    public static void serverSocket() throws IOException {
        System.out.println("\n[Connect Server Socket]");

        ss = new ServerSocket(Integer.parseInt(inputPort));
        Socket s = ss.accept();

        //Receive
        DataInputStream dis = new DataInputStream(s.getInputStream());
        String str = dis.readUTF();
        System.out.println("Client: "+str);

        //Send
        DataOutputStream dout = new DataOutputStream(s.getOutputStream());
        dout.writeUTF(String.valueOf(allObj));
        dout.flush();
    }

    public static void main(String[] args) {
        try{
            readConfig();
            readData();
            serverSocket();
            ss.close();
        }catch(Exception e){
            System.out.println(e);
        }
        input.close();
    }
}
