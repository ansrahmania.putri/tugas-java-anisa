package tugas1;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import tugas2.Model;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.*;

public class Main {
    //staff
    static int idS;
    static String namaS;
    static int tunjanganPulsaS;
    static int gajiS;
    static int absensiS;
    static int tunjanganMakanS;
    static Staff staff;

    //manager
    static int idM;
    static String namaM;
    static int tunjanganPulsaM;
    static int gajiM;
    static int absensiM;
    static int tunjanganTransportM;
    static int tunjanganEntertainM;
    static Manager manager;

    static StringBuilder strAccu;
    static JSONArray jSArray;
    static JSONArray jMArray;

    public static void main(String[] args) throws IOException, ParseException {
        Scanner input = new Scanner(System.in);
        int pilihan =0;
        int pilihan2 = 0;
        int pilihan3=0;
        int pilihan4=0;


        JSONObject objM;
        JSONObject objS;

        ArrayList<Manager> managerList = new ArrayList<>();
        ArrayList<Staff> staffList = new ArrayList<>();
        ArrayList<String> email = new ArrayList<>();
        ArrayList<Long> telepon = new ArrayList<>();

        do {
            System.out.println("MENU UTAMA");
            System.out.println("1.  Input Data");
            System.out.println("2.  Create JSON and Write to File to .txt");
            System.out.println("3.  Read JSON");
            System.out.println("4.  Tampilkan Data");
            System.out.println("99.  EXIT");
            System.out.print("Pilih Menu (1/2/3/4): ");
            pilihan = input.nextInt();

            switch (pilihan) {
                case 1:
                    do {
                        System.out.println("\nINPUT DATA");
                        System.out.println("1.  Staff");
                        System.out.println("2.  Manager");
                        System.out.println("99. Kembali ke MENU UTAMA");
                        System.out.print("Pilih Menu (1/2/99): ");
                        pilihan2 = input.nextInt();

                        switch (pilihan2) {
                            case 1:
                                for (int i = 0; i < pilihan2; i++) {
                                    System.out.print("\nID: ");
                                    int idS = input.nextInt();
                                    System.out.print("Nama: ");
                                    String namaS = input.next();
                                    System.out.print("Tunjangan Pulsa: ");
                                    int tunjanganPulsaS = input.nextInt();
                                    System.out.print("Gaji: ");
                                    int gajiS = input.nextInt();
                                    System.out.print("Absensi: ");
                                    int absensiS = input.nextInt();
                                    System.out.print("Tunjangan Makan: ");
                                    int tunjanganMakanS = input.nextInt();
                                    System.out.print("Email 1: ");
                                    String inputEmail1S = input.next();
                                    System.out.print("Email 2: ");
                                    String inputEmail2S = input.next();

                                    //add email list
                                    email.add(inputEmail1S);
                                    email.add(inputEmail2S);
                                    Iterator<String> emailIter = email.iterator();

                                    staff = new Staff(idS, namaS, tunjanganPulsaS, gajiS, absensiS, tunjanganMakanS, email);
                                    staffList.add(staff);
                                }
                                break;
                            case 2:
                                    System.out.print("\nID: ");
                                    int idM = input.nextInt();
                                    System.out.print("Nama: ");
                                    String namaM = input.next();
                                    System.out.print("Tunjangan Pulsa: ");
                                    int tunjanganPulsaM = input.nextInt();
                                    System.out.print("Gaji: ");
                                    int gajiM = input.nextInt();
                                    System.out.print("Absensi: ");
                                    int absensiM = input.nextInt();
                                    System.out.print("Tunjangan Transport: ");
                                    int tunjanganTransportM = input.nextInt();
                                    System.out.print("Tunjangan Entertain: ");
                                    int tunjanganEntertainM = input.nextInt();
                                    System.out.print("Telepon 1: ");
                                    long inputTelepon1M = input.nextLong();
                                    System.out.print("Telepon 2: ");
                                    long inputTelepon2M = input.nextLong();

                                    telepon.add(inputTelepon1M);
                                    telepon.add(inputTelepon2M);
                                    Iterator<Long> teleponIter = telepon.iterator();

                                    manager = new Manager(idM, namaM, tunjanganPulsaM, gajiM, absensiM, tunjanganTransportM, tunjanganEntertainM, telepon);
                                    managerList.add(manager);
                                break;
                            default:
                                if (pilihan2 == 99) {
                                    break;
                                } else {
                                    System.out.println("Menu yang anda masukkan tidak tersedia");
                                }
                        }
                    } while (pilihan2 != 99);
                break;
                case 2:
                    do {
                        System.out.println("\nCREATE JSON AND FILE.TXT");
                        System.out.println("1.  Staff");
                        System.out.println("2.  Manager");
                        System.out.println("99. Kembali ke MENU UTAMA");
                        System.out.print("Pilih Menu (1/2/99): ");
                        pilihan3 = input.nextInt();

                        switch (pilihan3) {
                            case 1:
                                jSArray = new JSONArray();
                                for (Staff s : staffList) {
                                    objS = new JSONObject();
                                    objS.put("ID", s.getId());
                                    objS.put("Nama", s.getNama());
                                    objS.put("Tunjangan Pulsa", s.getTunjanganPulsa());
                                    objS.put("Gaji", s.getGaji());
                                    objS.put("Absensi", s.getAbsensi());
                                    objS.put("Tunjangan Makan", s.getTunjanganMakan());
                                    JSONArray jsonEmail = new JSONArray();
                                    jsonEmail.add(email);
                                    objS.put("Email", jsonEmail);
                                    jSArray.add(objS);

                                }
                                System.out.println("\n[Tambah Data ke File Baru]");

                                String newFileS = "/Users/ada-nb187/Documents/tugasd8/src/tugas1/fileWriterS.txt";

                                try {
                                    FileWriter fw = new FileWriter(newFileS);
                                    fw.write(String.valueOf(jSArray));
                                    fw.close();
                                } catch (Exception e) {
                                    System.out.println(e);
                                }

                                System.out.println("Data Staff Berhasil Ditambahkan!");
                                break;
                            case 2:
                                jMArray = new JSONArray();
                                objM = new JSONObject();
                                for (Manager m : managerList) {
                                    objM.put("ID", m.getId());
                                    objM.put("Nama", m.getNama());
                                    objM.put("Tunjangan Pulsa", m.getTunjanganPulsa());
                                    objM.put("Gaji", m.getGaji());
                                    objM.put("Absensi", m.getAbsensi());
                                    objM.put("Tunjangan Transport", m.getTunjanganTransport());
                                    objM.put("Tunjangan Entertainment", m.getTunjanganEntertain());
                                    JSONArray jsonTelp = new JSONArray();
                                    jsonTelp.add(telepon);
                                    objM.put("Telepon", jsonTelp);
                                    jMArray.add(objM);

                                }
                                System.out.println("\n[Tambah Data ke File Baru]");

                                String newFileM = "/Users/ada-nb187/Documents/tugasd8/src/tugas1/fileWriterM.txt";

                                try {
                                    FileWriter fw = new FileWriter(newFileM);
                                    fw.write(String.valueOf(objM));
                                    fw.close();
                                } catch (Exception e) {
                                    System.out.println(e);
                                }
                                System.out.println("Data Manager Berhasil Ditambahkan!");
                                break;
                            default:
                                if (pilihan3==99) {
                                    break;
                                } else {
                                    System.out.println("Menu yang anda masukkan tidak tersedia");
                                }
                        }
                    } while (pilihan3 != 99);
                break;
                case 3:
                    do {
                        System.out.println("MENU STAFF DAN MANAGER");
                        System.out.println("1.  Staff");
                        System.out.println("2.  Manager");
                        System.out.println("99. Kembali ke MENU UTAMA");
                        System.out.print("Pilih Menu (1/2/99): ");
                        pilihan4 = input.nextInt();

                        int i;
                        JSONParser parser;
                        Reader reader;
                        Object jsonObj;
                        JSONObject jsonObject;

                        switch (pilihan4) {
                            case 1:
                                String fileS = "/Users/ada-nb187/Documents/tugasd8/src/tugas1/fileWriterS.txt";

                                FileReader frS = new FileReader(fileS);


                                strAccu = new StringBuilder();
                                while((i=frS.read()) != -1) {
                                    strAccu.append((char) i);
                                }
                                System.out.println(strAccu);

                                //decode JSON
                                parser = new JSONParser();
                                reader = new StringReader(String.valueOf(strAccu));

                                jsonObj = parser.parse(reader);

                                jsonObject = (JSONObject) jsonObj;

                                long idSJSON = (long) jsonObject.get("ID");
                                System.out.println("ID = " + idSJSON);
                                String namaSJSON = (String) jsonObject.get("Nama");
                                System.out.println("Nama = " + namaSJSON);
                                long tunjanganPulsaSJSON = (long) jsonObject.get("Tunjangan Pulsa");
                                System.out.println("Tunjangan Pulsa = " + tunjanganPulsaSJSON);
                                long gajiSJSON = (long) jsonObject.get("Gaji");
                                System.out.println("Gaji = " + gajiSJSON);
                                long absensiSJSON = (long) jsonObject.get("Absensi");
                                System.out.println("Absensi = " + absensiSJSON);
                                long tunjanganMakanSJSON = (long) jsonObject.get("Tunjangan Makan");
                                System.out.println("Tunjangan Makan = " + tunjanganMakanSJSON);

                                ArrayList<JSONArray> emailJSON = (ArrayList<JSONArray>) jsonObject.get("Email");
                                for (JSONArray elS : emailJSON) {
                                    System.out.println("Email = " + elS);
                                }
                            break;
                            case 2:
                                String fileM = "/Users/ada-nb187/Documents/tugasd8/src/tugas1/fileWriterM.txt";

                                FileReader frM = new FileReader(fileM);

                                strAccu = new StringBuilder();
                                while((i=frM.read()) != -1) {
                                    strAccu.append((char) i);
                                }
                                System.out.println(strAccu);

                                //decode JSON
                                parser = new JSONParser();
                                reader = new StringReader(String.valueOf(strAccu));

                                jsonObj = parser.parse(reader);

                                jsonObject = (JSONObject) jsonObj;

                                long idMJSON = (long) jsonObject.get("ID");
                                System.out.println("ID = " + idMJSON);
                                String namaMJSON = (String) jsonObject.get("Nama");
                                System.out.println("Nama = " + namaMJSON);
                                long tunjanganPulsaMJSON = (long) jsonObject.get("Tunjangan Pulsa");
                                System.out.println("Tunjangan Pulsa = " + tunjanganPulsaMJSON);
                                long gajiMJSON = (long) jsonObject.get("Gaji");
                                System.out.println("Gaji = " + gajiMJSON);
                                long absensiMJSON = (long) jsonObject.get("Absensi");
                                System.out.println("Absensi = " + absensiMJSON);
                                long tunjanganMakanMJSON = (long) jsonObject.get("Tunjangan Transport");
                                System.out.println("Tunjangan Transport = " + tunjanganMakanMJSON);

                                ArrayList<JSONArray> teleponJSON = (ArrayList<JSONArray>) jsonObject.get("Telepon");
                                for (JSONArray elM : teleponJSON) {
                                    System.out.println("Telepon = " + elM);
                                }
                            break;
                            default:
                                if (pilihan4==99) {
                                    break;
                                } else {
                                    System.out.println("Menu yang anda pilih tidak tersedia");
                                }
                        }
                break;
                    } while (pilihan4!=99);
                case 4:
                    for (Staff s: staffList) {
                        System.out.println(s.getId()+s.getNama()+s.getGaji()+s.getAbsensi()+s.getTunjanganMakan()+s.getTunjanganPulsa()+s.getEmail());
                    }
                    System.out.println();
        }
    }while (pilihan != 99);
}
}
