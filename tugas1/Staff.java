package tugas1;

import java.util.ArrayList;

public class Staff extends Worker{
    int tunjanganMakan;
    ArrayList<String> email = new ArrayList<>();


    public Staff(int id, String nama, int tunjanganPulsa, int gaji, int absensi, int tunjanganMakan, ArrayList<String> email) {
        super(id, nama, tunjanganPulsa, gaji, absensi);
        this.tunjanganMakan = tunjanganMakan;
        this.email = email;
    }

    public int getTunjanganMakan() {
        return tunjanganMakan;
    }

    public void setTunjanganMakan(int tunjanganMakan) {
        this.tunjanganMakan = tunjanganMakan;
    }

    public ArrayList<String> getEmail() {
        return email;
    }

    public void setEmail(ArrayList<String> email) {
        this.email = email;
    }
}
