package tugas1;

import java.util.ArrayList;

public class Manager extends Worker{

    int tunjanganTransport;
    int tunjanganEntertain;

    ArrayList<Long> telepon = new ArrayList<>();


    public Manager(int id, String nama, int tunjanganPulsa, int gaji, int absensi, int tunjanganTransport, int tunjanganEntertain, ArrayList<Long> telepon) {
        super(id, nama, tunjanganPulsa, gaji, absensi);
        this.tunjanganTransport = tunjanganTransport;
        this.tunjanganEntertain = tunjanganEntertain;
        this.telepon = telepon;
    }

    public int getTunjanganTransport() {
        return tunjanganTransport;
    }

    public void setTunjanganTransport(int tunjanganTransport) {
        this.tunjanganTransport = tunjanganTransport;
    }

    public int getTunjanganEntertain() {
        return tunjanganEntertain;
    }

    public void setTunjanganEntertain(int tunjanganEntertain) {
        this.tunjanganEntertain = tunjanganEntertain;
    }

    public ArrayList<Long> getTelepon() {
        return telepon;
    }

    public void setTelepon(ArrayList<Long> telepon) {
        this.telepon = telepon;
    }
}
