package tugas1;

public abstract class Worker {
    int id;
    String nama;
    int tunjanganPulsa;
    int gaji;
    int absensi;

    public Worker(int id, String nama, int tunjanganPulsa, int gaji, int absensi) {
        this.id = id;
        this.nama = nama;
        this.tunjanganPulsa = tunjanganPulsa;
        this.gaji = gaji;
        this.absensi = absensi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getTunjanganPulsa() {
        return tunjanganPulsa;
    }

    public void setTunjanganPulsa(int tunjanganPulsa) {
        this.tunjanganPulsa = tunjanganPulsa;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }
}
